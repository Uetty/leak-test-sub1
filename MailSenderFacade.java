public class MailSenderFacade implements InitializingBean {

    private Logger LOG = LoggerFactory.getLogger(MailSenderFacade.class);

    private JavaEmailSender emailSender;
    @Autowired
    AsyncMailProducer mailProducer;
    @Autowired
    ConverterSelector converterSelector;
    @Autowired
    MailTaskInfoDao mailTaskInfoDao;

    private MessageSendListener messageListener;

    @Override
    public void afterPropertiesSet() {
        MailConfig mailConfig = new MailConfig();
        this.emailSender = new JavaEmailSender(mailConfig.getUsername(), mailConfig.getPassword(), mailConfig.getHost(),
                mailConfig.getPort(), mailConfig.getUseSSL(), mailConfig.getUseSTARTTLS());

        converterSelector.addConverter(MailClassify.COMMON, new CommonEntityConverter());
        converterSelector.addConverter(MailClassify.COMMON_FTL, new CommonFtlEntityConverter());
    }


    /**
     * 同步发送邮件
     * @param message 邮件内容信息
     * @return : com.secfox.store.vo.mail.MailTaskInfo 从返回任务是否为空、任务状态来判断是否成功
     */
    @SuppressWarnings({"unchecked", "UnusedReturnValue"})
    public MailTaskInfo syncSend(MessageEntity message) {
        MailInfo mailInfo = null;
        MailTaskInfo task = null;
        try {
            assertAddress(message);

            task = createTask(message, MailSendMode.SYNC);

            MessageEntityConverter converter = converterSelector.getConverter(message.getClassify());
            Objects.requireNonNull(converter);

            mailInfo = converter.convertToMailInfo(message);
            emailSender.sendMail(mailInfo);

            onSuccess(task,mailInfo);
        } catch (MailSendException e) {
            onError(task,mailInfo,e.getCause());
        }
        return task;
    }

    /**
     * 异步发送邮件
     * @param message 消息
     * @return : 是否成功

     */
    @SuppressWarnings("UnusedReturnValue")
    public boolean asyncSend(MessageEntity message) {
        MailTaskInfo task;
        try {
            assertAddress(message);

            task = createTask(message, MailSendMode.ASYNC);

            mailProducer.sendMessage(task.getId(),0);
            return true;
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            return false;
        }
    }

    /**
     * 邮件地址校验及去重
     * @param message 邮件信息
     */
    private void assertAddress(MessageEntity message) {
        List<String> toUsers = message.getToUsers();
        distinctAndAssertAddress(toUsers);

        List<String> ccUsers = message.getCcUsers();
        distinctAndAssertAddress(ccUsers);

        List<String> bccUsers = message.getBccUsers();
        distinctAndAssertAddress(bccUsers);

        if (toUsers.size() == 0 && ccUsers.size() == 0 && bccUsers.size() == 0) {
            throw new RuntimeException("to/cc/bcc users can not both empty");
        }
    }

    private void distinctAndAssertAddress(List<String> list) {
        if (list == null) return;
        Set<String> tempList = list.stream()
                .filter(StringUtils::isNotBlank)
                .map(String::trim)
                .map(String::toLowerCase)
                .collect(Collectors.toSet());
        for (String mailAddr : tempList) {
            if (!StringUtil.checkEmail(mailAddr)) {
                throw new RuntimeException("email address invalid [" + mailAddr + "]");
            }
        }
        list.clear();
        list.addAll(tempList);
    }

    /**
     * 从数据库中获取邮件任务并发送（异步邮件的触发放这里）
     * @param taskId 任务id
     * @return : com.secfox.store.vo.mail.MailTaskInfo
     */
    @SuppressWarnings({"UnusedReturnValue", "unchecked"})
    public MailTaskInfo sendScheduledTask(String taskId) {
        MailInfo mailInfo = null;
        MailTaskInfo task = null;
        try {
            task = mailTaskInfoDao.getById(taskId);
            if (task.getStatus() == MailTaskStatus.SUCCESS || task.getStatus() == MailTaskStatus.FAILED) {
                return task;
            }

            MessageEntityConverter converter = converterSelector.getConverter(task.getClassify());
            Objects.requireNonNull(converter);
            MessageEntity messageEntity = converter.convertFromTask(task);
            mailInfo = converter.convertToMailInfo(messageEntity);

            emailSender.sendMail(mailInfo);

            onSuccess(task,mailInfo);
        } catch (MailSendException e) {
            onError(task,mailInfo,e.getCause());
        }
        return task;
    }

    @SuppressWarnings("unchecked")
    private MailTaskInfo createTask(MessageEntity message, MailSendMode sendMode) {
        MessageEntityConverter converter = converterSelector.getConverter(message.getClassify());
        Objects.requireNonNull(converter);

        MailTaskInfo mailTaskInfo = converter.convertToTaskInfo(message);
        mailTaskInfo.setId(UUID.randomUUID().toString());
        mailTaskInfo.setStatus(MailTaskStatus.PENDING);
        mailTaskInfo.setSendMode(sendMode);

        mailTaskInfoDao.create(mailTaskInfo);
        mailTaskInfo = mailTaskInfoDao.getById(mailTaskInfo.getId());
        return mailTaskInfo;
    }

    private void onSuccess(MailTaskInfo mailTaskInfo, MailInfo mailInfo) {
        mailTaskInfo.setStatus(MailTaskStatus.SUCCESS);
        mailTaskInfoDao.update(mailTaskInfo);

        if (messageListener != null) {
            messageListener.onSuccess(mailTaskInfo,mailInfo);
        }
    }

    private void onError(MailTaskInfo mailTaskInfo, MailInfo mailInfo, Throwable e) {
        if (mailTaskInfo != null) {
            mailTaskInfo.setStatus(MailTaskStatus.FAILED);
            Integer errorCount = mailTaskInfo.getErrorCount();
            if (errorCount == null) errorCount = 0;
            mailTaskInfo.setErrorCount(errorCount + 1);
            mailTaskInfo.setErrorMsg(e.getMessage());
            mailTaskInfoDao.update(mailTaskInfo);
        }
        LOG.error(e.getMessage(), e);

        if (messageListener != null) {
            messageListener.onError(mailTaskInfo,mailInfo,e);
        }
    }

    @SuppressWarnings("unused")
    public MessageSendListener getMessageListener() {
        return messageListener;
    }

    @SuppressWarnings("unused")
    public void setMessageListener(MessageSendListener messageListener) {
        this.messageListener = messageListener;
    }
}